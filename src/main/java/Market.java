public class Market extends Thread {

    private CakeStore store;
    private Integer cakesOrder;
    private Integer cakesSales;

    Market(CakeStore store, Integer cakesOrder, Integer cakesSales) {
        this.store = store;
        this.cakesOrder = cakesOrder;
        this.cakesSales = cakesSales;
    }

    @Override
    public void run() {

        //supply of cakes
        store.requestCakes(CakeFactory.supplyCakes(store, cakesOrder));
        CakeFactory.showCakes(CakeFactory.cakes);

        //cakes sales
        System.out.println("Cakes in " + store.getName() + " BEFORE sales: ");
        store.showCakes(store.getCakesInStore());
        store.sellCakes(cakesSales);
        System.out.println("Cakes in " + store.getName() + " AFTER sales: ");
        store.showCakes(store.getCakesInStore());
    }
}
