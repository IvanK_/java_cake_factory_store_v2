import java.util.Arrays;
import java.lang.System;

public class CakeStore {

    private String name;
    private Cake[] cakesInStore;

    public CakeStore(String name) {
        this.name = name;
        this.cakesInStore = new Cake[15];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Cake[] getCakesInStore() {
        return cakesInStore;
    }

    public Integer getNumberOfCakesInStore() {
        Integer readyCakes = 0;
        for (int i=0; i < cakesInStore.length; i++) {
            if (cakesInStore[i]==null) break;
            readyCakes++;
        }
        return readyCakes;
    }

    public void setCakesInStore(Cake[] cakesInStore) {
        this.cakesInStore = cakesInStore;
    }

    //check availability of place in the Cake Factory
    public Integer checkAvailablePlaces() {
        Integer free=0;

        for (int i=0; i<cakesInStore.length; i++) {
            if (cakesInStore[i]==null) free++;
        }
        return free;
    }

    //Store requests from Cake Factory an array of cakes
    //as argument of this function should be transmited CakeFactory.supplyCakes(Integer quantityRequested)
    public void requestCakes(Cake[] cakes) {
        Integer availablePlaces = checkAvailablePlaces();
        Integer suppliedCakes = cakes.length;
        Integer numberOfCakesToBeReturnedToFactory = 0;
        Integer numberOfCakesToBeAddedToTheStore=0;

        System.out.println("[" + getName() + "]. Available place in store: " + availablePlaces);
        System.out.println("[" + getName() + "]. Number of cakes arrived from the Factory: " + suppliedCakes);

        if (suppliedCakes > availablePlaces) {
            numberOfCakesToBeReturnedToFactory = suppliedCakes-availablePlaces;
            numberOfCakesToBeAddedToTheStore = availablePlaces;
        } else numberOfCakesToBeAddedToTheStore=suppliedCakes;

        if (numberOfCakesToBeReturnedToFactory!=0) CakeFactory.cookCakes(numberOfCakesToBeReturnedToFactory);          //we put them back on the Factory's shelf

        //placing new arrived cakes on the Store's shelf
        int start = cakesInStore.length - checkAvailablePlaces();
        int stop = start + numberOfCakesToBeAddedToTheStore;
        System.out.println("[" + getName() + "]. Placing new cakes in the store: " + numberOfCakesToBeAddedToTheStore +" cakes.");
        cakesInStore = Arrays.copyOfRange(cakes, start, stop); //->>> Using Arrays.copyOfRange() instead of using the below "for"
        //for (int i=start, j=0; i<stop||j<numberOfCakesToBeAddedToTheStore; i++, j++)
        //    cakesInStore[i] = cakes[j];
        System.out.println();
    }

    public void sellCakes(Integer number) {

        Integer availableCakes = getNumberOfCakesInStore();
        Integer cakesToBeSold=0;

        if (availableCakes < number) cakesToBeSold = availableCakes;
        else cakesToBeSold = number;
        System.out.println("[" + getName() + "] sells " + cakesToBeSold + " cakes");
        Arrays.fill(cakesInStore, availableCakes-cakesToBeSold, availableCakes, null);      //-->> Using Arrays.fill instead of the next "for"
        //for (int i=(availableCakes-1), j=0; j<cakesToBeSold; i--, j++) {
        //       cakesInStore[i]=null;
        //}
    }

    //show cakes in the Store
    public void showCakes(Cake cake[]) {
        System.out.println("[" + getName() + "]. Show cakes in the Store: ");
        for (int i=0; i<cake.length; i++)
            System.out.println("["+(i+1)+"]: "+cake[i]);
        System.out.println();
    }

    @Override
    public String toString() {
        return "CakeStore [name=" + name + ", cakesInStore=" + Arrays.toString(cakesInStore) + "]";
    }



}