public class Application {
    public static void main(String[] args) {

        //Factory cooks a quantity of cakes
        CakeFactory.cookCakes(20);
        CakeFactory.showCakes(CakeFactory.cakes);

        //Stores are created
        CakeStore store1 = new CakeStore("Store 1");
        CakeStore store2 = new CakeStore("Store 2");

        //Markets are created: name, quantity_to_be_ordered, quantity_to_be_sold
        Market marketGreenHills = new Market(store1,10,7);
        Market marketBonus = new Market(store2,18,9);

        //Start the process: ordering, supplying and selling
        marketGreenHills.run();
        marketBonus.run();
    }
}
