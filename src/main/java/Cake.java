public class Cake {
    private String name;
    private Integer gramms;
    private Integer expires;
    private Float price;


    public Cake(String name, Integer gramms, Integer expires, Float price) {
        this.setName(name);
        this.setExpires(expires);
        this.setPrice(price);
        this.setGramms(gramms);
    }
    public Cake () {

    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getGramms() {
        return gramms;
    }
    public void setGramms(Integer gramms) {
        this.gramms = gramms;
    }
    public Integer getExpires() {
        return expires;
    }
    public void setExpires(Integer expires) {
        this.expires = expires;
    }
    public Float getPrice() {
        return price;
    }
    public void setPrice(Float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Cake [name=" + name + ", gramms=" + gramms + ", expires=" + expires + ", price=" + price + "]";
    }
}
