import java.lang.Object;
import java.util.Arrays;
public class CakeFactory {

    static String name = "Happiness";
    static Cake[] cakes = new Cake[20]; //maximum limit of cakes in factory

    //prepare cakes
    public static void cookCakes(Integer quantity) {
        if (quantity>checkAvailablePlaces()) System.err.println("Factory can't accept more then " + cakes.length + "cakes.");
        else {
            int start = cakes.length - checkAvailablePlaces();
            int stop = start + quantity;
            System.out.println("Factory cooks " + quantity +" cakes.");
            for (int i=start; i<stop; i++) {
                Arrays.fill(cakes, 0, cakes.length, new Cake("Donat Coffee - " +i, 300, 10, 15.5f));    //Using ARRAY class
                //cakes[i]=new Cake("Donat Coffee - " +i, 300, 10, 15.5f);
            }
        }
        System.out.println();
    }

    //show cakes in the Factory
    public static void showCakes(Cake cake[]) {
        System.out.println("Factory balance of cakes: ");
        for (int i=0; i<cake.length; i++)
            System.out.println("["+(i+1)+"]: "+cake[i]);
        System.out.println();
    }

    //check availability of place in the Cake Factory
    public static Integer checkAvailablePlaces() {
        Integer free=0;
        for (int i=0; i<cakes.length; i++) {
            if (cakes[i]==null) free++;
        }
        return free;
    }

    //get Total cost of cakes we have on the shelf
    public static float getTotalCost() {
        float TotalCost = 0f;
        for (int i=0; i<cakes.length; i++)
            if(cakes[i]!=null) TotalCost+=cakes[i].getPrice();
        return  TotalCost;
    }

    public static Integer getNumberOfCakesInfactory() {
        Integer readyCakes = 0;
        for (int i=0; i < cakes.length; i++) {
            if (cakes[i]==null) break;
            readyCakes++;
        }
        return readyCakes;
    }

    //supply cakes (take them off of the shelf)
    public static Cake[] supplyCakes(CakeStore store, Integer quantityRequested) {
        Integer numberOfReadyCakes =0;
        Integer quantityToBeSupplied = 0;
        if (quantityRequested > getNumberOfCakesInfactory()) {
            System.out.println("Factory -> [" + store.getName() + "]: You requested too many cakes!");
            System.out.println("Factory -> [" + store.getName() + "]. Cakes requested: " + quantityRequested);
            System.out.println("Factory -> [" + store.getName() + "]. Cakes available: " + getNumberOfCakesInfactory());
            quantityRequested=getNumberOfCakesInfactory();
        }
        //get number of ready cakes on the shelf
        for (int i=0; i<cakes.length; i++) {
            if (cakes[i]!=null) numberOfReadyCakes++;
        }

        quantityToBeSupplied = (quantityRequested > numberOfReadyCakes) ? numberOfReadyCakes : quantityRequested;
        System.out.println("Factory -> [" + store.getName() + "]" + " Quantity to be supplied to the store is: " + quantityToBeSupplied);

        //initialize array of cakes to be supplied
        Cake[] temp = new Cake[quantityToBeSupplied];
        for (int i=0; i<temp.length; i++) temp[i]= new Cake();

        //do not look through the all array of cakes, because we already know the number of cakes on the shelf
        //prepare array of cakes to be supplied
        //temp = Arrays.copyOfRange(cakes, 0, numberOfReadyCakes-1);
        //Arrays.fill(cakes, numberOfReadyCakes-1, numberOfReadyCakes-quantityToBeSupplied-1,null);
        for (int i=(numberOfReadyCakes-1), j=0; i>=0; i--, j++) {
            if (cakes[i]!=null) {
                temp[j]=cakes[i];
                cakes[i]=null;
                if ((j+1)==quantityToBeSupplied) break;
                else continue;
            }
        }
        System.out.println("Factory -> [" + store.getName() + "]" +" As result of the Order processing you will get " + temp.length + " cakes.");
        System.out.println();

        return temp;
    }
}
